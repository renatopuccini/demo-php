pipeline{
    agent{
        node{
            label "maven"
        }
    }
    
    environment{
        APPLICATION_NAME = 'demo-php'
        DEV_PROJECT = "demo-bitbucket"
        BUILDCFG_NAME = "demo-php"
        GIT = "https://renatopuccini@bitbucket.org/renatopuccini/demo-php.git"
    }
    
    stages{
        stage('Delete buildconfig'){
            steps {
                script {
                    openshift.withCluster() {
                        openshift.withProject(DEV_PROJECT) {
                            openshift.selector("all", [ template : BUILDCFG_NAME ]).delete()
                        }
                    }
                }
            }
        }

        stage('Create Image Builder') {
            when {
                expression {
                    openshift.withCluster() {
                        openshift.withProject(DEV_PROJECT) {
                            return !openshift.selector("bc", "${BUILDCFG_NAME}").exists();
                        }
                    }
                }
            }
           steps {
                script {
                    openshift.withCluster() {
                        openshift.withProject(DEV_PROJECT) {
                            openshift.newBuild("--name=${BUILDCFG_NAME}", "--image-stream=openshift/php:latest", GIT).logs("-f")
                        }
                    }
                }
            }
        }
    }

   
    
}
